/**
 * Elastic Search Options for connecting to the elastic search endpoint.
 */
export interface ElasticSearchDatabaseOptions {
  /**
   * The elastic search endpoint url.
   */
  readonly url: string;
}
