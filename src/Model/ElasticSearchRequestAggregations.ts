/**
 * @see https://www.elastic.co/guide/en/elasticsearch/reference/current/search-aggregations.html
 */
export type ElasticSearchRequestAggregations = Record<
  string,
  ElasticSearchRequestTermAggregations
>;

/**
 * @see https://www.elastic.co/guide/en/elasticsearch/reference/current/search-aggregations-bucket-terms-aggregation.html
 */
export type ElasticSearchRequestTermAggregations = {
  term: ElasticSearchRequestTermAggregationsOptions;
};

/**
 * @see https://www.elastic.co/guide/en/elasticsearch/reference/current/search-aggregations-bucket-terms-aggregation.html
 */
export type ElasticSearchRequestTermAggregationsOptions = {
  field: string;
  size?: number;
  order?: Record<string, "asc" | "desc">;
  min_doc_count?: number;
  include?: string | string[];
  exclude?: string | string[];
};
