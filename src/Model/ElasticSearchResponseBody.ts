export type ElasticSearchResponseBodyHit<TSource> = {
  _index: string;
  _type: string;
  _id: string;
  _score: number;
  _source: TSource;
};

export type ElasticSearchResponseBodyAggregation = {
  doc_count_error_upper_bound: number;
  sum_other_doc_count: number;
  buckets: {
    key: string;
    doc_count: number;
  }[];
};

export type ElasticSearchResponseBodyFilteredAggregation = {
  doc_count: number;
  _filtered: ElasticSearchResponseBodyAggregation;
};

export type ElasticSearchResponseBodyAggregations = Record<
  string,
  | ElasticSearchResponseBodyAggregation
  | ElasticSearchResponseBodyFilteredAggregation
>;

export type ElasticSearchResponseBody<TSource> = {
  hits: {
    hits: ElasticSearchResponseBodyHit<TSource>[];
    total: {
      value: number;
      relation: "eq";
    };
  };
  took: number;
  timed_out: boolean;
  aggregations?: ElasticSearchResponseBodyAggregations;
};
