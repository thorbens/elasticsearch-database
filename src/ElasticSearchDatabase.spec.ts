import { DatabaseModel } from "@thorbens/database";
import { ElasticSearchDatabase } from "./ElasticSearchDatabase";
import { ElasticSearchDatabaseOptions } from "./Model";

class MockModel implements DatabaseModel {
  public id!: string;
}

describe("ElasticSearchDatabase", () => {
  it("should return a repository for the given type", async () => {
    const options: ElasticSearchDatabaseOptions = {
      url: "http://elasticsearch:9200",
    };
    const database = new ElasticSearchDatabase(options);
    const repository = await database.getRepository(MockModel);
    expect(repository).not.toEqual(null);

    // disconnect from database
    await database.disconnect();
  });
});
