import {
  AbstractDatabaseConditionParser,
  DatabaseConditionBuilder,
} from "@thorbens/database";
import { ElasticSearchDatabaseConditionBuilder } from "./ElasticSearchDatabaseConditionBuilder";

export class ElasticSearchDatabaseConditionParser<
  TModel = {}
> extends AbstractDatabaseConditionParser<TModel> {
  private readonly conditionBuilder: ElasticSearchDatabaseConditionBuilder<TModel>;

  public constructor(
    conditionBuilder: ElasticSearchDatabaseConditionBuilder<TModel>
  ) {
    super();
    this.conditionBuilder = conditionBuilder;
  }

  protected getConditionBuilder(): DatabaseConditionBuilder<TModel> {
    return this.conditionBuilder;
  }

  protected isDate(): boolean {
    return false;
  }
}
