type FieldTypes =
  | "text"
  | "keyword"
  | "date"
  | "boolean"
  | "ip"
  | "completion"
  | "nested"
  | "object"
  // numeric values, @see https://www.elastic.co/guide/en/elasticsearch/reference/current/number.html
  | "long"
  | "unsigned_long"
  | "float"
  | "half_float"
  | "scaled_float"
  | "double"
  | "short"
  | "byte";

export interface ElasticSearchFieldMapping<T = unknown> {
  type: FieldTypes;
  /**
   * The analyzer for this field.
   *
   * @see https://www.elastic.co/guide/en/elasticsearch/reference/current/analyzer.html
   */
  analyzer?: string;
  fields?: Record<
    string,
    {
      type: FieldTypes;
      ignore_above?: number;
    }
  >;
  /**
   * The index option controls whether field values are indexed. It accepts true or false and defaults to true.
   * Fields that are not indexed are not queryable.
   */
  index?: boolean;
  /**
   * The enabled setting, which can be applied only to the top-level mapping definition and to object fields,
   * causes Elasticsearch to skip parsing of the contents of the field entirely. The JSON can still be retrieved
   * from the _source field, but it is not searchable or stored in any other way:
   */
  enabled?: boolean;
  /**
   * Type mappings, object fields and nested fields contain sub-fields, called properties.
   * These properties may be of any data type, including object and nested.
   *
   * @see https://www.elastic.co/guide/en/elasticsearch/reference/current/properties.html
   */
  properties?: PropertiesMapping<T>;
}

export interface ElasticSearchMapping {
  properties?: Record<string, ElasticSearchFieldMapping>;
}

export interface ElasticSearchMappingOptions {
  indexName?: string;
  /**
   * @see https://www.elastic.co/guide/en/elasticsearch/reference/7.14/index-modules.html
   */
  indexOptions?: any; // as the settings may be deeply nested and complex, use any for now.
  /**
   * Additional non-keyword fields which may not be covered by {@link FieldMapping}
   * annotations (e.g. it is currently not possible for nested objects.
   */
  additionalNonKeywordsFields?: ReadonlyArray<string>;
}

export interface ElasticSearchMappingProvider {
  /**
   * Returns the mapping for the index. If no mapping is returned, the mapping will be created automatically by the
   * elastic search upon indexing documents.
   */
  getMapping(): ElasticSearchMapping | undefined;

  /**
   * Returns the mapping options.
   */
  getMappingOptions(): ElasticSearchMappingOptions;
}

export type PropertiesMapping<T> = Partial<
  Record<keyof T, ElasticSearchFieldMapping>
>;

export function isElasticSearchMappingProvider(
  input: unknown
): input is ElasticSearchMappingProvider {
  return typeof input === "object" && input != null && "getMapping" in input;
}
