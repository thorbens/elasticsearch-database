import {
  AbstractLogicalDatabaseOperator,
  CommonDatabaseOperatorObject,
  CommonDatabaseOperatorType,
  LogicalDatabaseOperator,
} from "@thorbens/database";
import { ElasticSearchDatabaseConditionBuilder } from "./ElasticSearchDatabaseConditionBuilder";
import { ElasticSearchDatabaseConditionParser } from "./ElasticSearchDatabaseConditionParser";
import { AndDatabaseOperator, OrDatabaseOperator } from "./Operators";

describe("ElasticSearchDatabaseConditionParser", () => {
  it("should parse an and condition correctly", () => {
    const conditionParser = new ElasticSearchDatabaseConditionParser(
      new ElasticSearchDatabaseConditionBuilder()
    );
    const value = "foo";
    const type = CommonDatabaseOperatorType.AND;
    const condition: CommonDatabaseOperatorObject = {
      type,
      value,
    };

    const parsedCondition = conditionParser.parse(condition);
    if (!parsedCondition) {
      throw new Error(`parsing should not return null`);
    }

    expect(parsedCondition.getValue()).toEqual(value);
    expect(parsedCondition.getType()).toEqual(type);
    expect(parsedCondition instanceof AbstractLogicalDatabaseOperator).toEqual(
      true
    );
  });

  it("should parse nested condition correctly", () => {
    const conditionParser = new ElasticSearchDatabaseConditionParser(
      new ElasticSearchDatabaseConditionBuilder()
    );
    const condition: CommonDatabaseOperatorObject = {
      type: CommonDatabaseOperatorType.AND,
      value: [
        {
          type: "or",
          value: [
            {
              key: "type",
              type: CommonDatabaseOperatorType.EQUAL,
              value: "MOVIE",
            },
          ],
        },
      ],
    };

    const parsedCondition = conditionParser.parse(condition);
    if (!parsedCondition) {
      throw new Error(`parsing should not return null`);
    }

    expect(parsedCondition instanceof AndDatabaseOperator).toEqual(true);
    expect(parsedCondition.getType()).toEqual(CommonDatabaseOperatorType.AND);
    // get the value of the parsed and condition
    const andConditionValue = parsedCondition.getValue() as LogicalDatabaseOperator<unknown>[];
    // there should be one value
    expect(andConditionValue.length).toEqual(1);
    // get the or condition
    const parsedOrCondition = andConditionValue[0];
    expect(parsedOrCondition instanceof OrDatabaseOperator).toEqual(true);
    expect(parsedOrCondition.getType()).toEqual(CommonDatabaseOperatorType.OR);
  });
});
