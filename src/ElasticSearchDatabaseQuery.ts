import { AbstractDatabaseQuery, DatabaseOperator } from "@thorbens/database";

/**
 * An Elasticsearch database query which allows additional
 * operations such as {@link https://www.elastic.co/guide/en/elasticsearch/reference/7.13/filter-search-results.html#post-filter}.
 */
export class ElasticSearchDatabaseQuery<
  TModel,
  TResult = TModel
> extends AbstractDatabaseQuery<TModel, TResult> {
  private readonly postConditions: DatabaseOperator[] = [];

  /**
   * Adds a new post condition.
   *
   * @param condition The condition to add.
   */
  public addPostCondition(condition: DatabaseOperator) {
    this.postConditions.push(condition);
  }

  /**
   * Returns the current post conditions.
   */
  public getPostConditions(): ReadonlyArray<DatabaseOperator> {
    return this.postConditions;
  }

  /**
   * Creates the post conditions on database level
   * (see {@link AbstractDatabaseQuery#toObject}).
   */
  public createPostConditions() {
    return this.getPostConditions().reduce(
      (obj, condition) => ({
        ...obj,
        ...condition.toObject(),
      }),
      {}
    );
  }
}
