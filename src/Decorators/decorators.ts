import "reflect-metadata";
import {
  ElasticSearchFieldMapping,
  ElasticSearchMapping,
  ElasticSearchMappingOptions,
  ElasticSearchMappingProvider,
  PropertiesMapping,
} from "../mapping";

const META_DATA_KEY = "elasticsearch:fieldmapping";

const DEFAULT_MAPPING_OPTIONS: ElasticSearchMappingOptions = Object.freeze({});

// eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
export function RepositoryMapping(
  mappingOptions: ElasticSearchMappingOptions = {}
) {
  mappingOptions = {
    ...DEFAULT_MAPPING_OPTIONS,
    ...mappingOptions,
  };

  // eslint-disable-next-line @typescript-eslint/ban-types, @typescript-eslint/no-explicit-any
  return <T extends new (...args: any[]) => {}>(constructor: T) => {
    if (!mappingOptions.indexName) {
      mappingOptions.indexName = constructor.name.toLowerCase();
    }

    return class extends constructor implements ElasticSearchMappingProvider {
      public getMappingOptions(): ElasticSearchMappingOptions {
        return mappingOptions;
      }

      public getMapping(): ElasticSearchMapping | undefined {
        return {
          properties: Object.keys(this).reduce((collector, key) => {
            const fieldMapping = Reflect.getMetadata(META_DATA_KEY, this, key);
            if (!fieldMapping) {
              return collector;
            }

            return {
              ...collector,
              [key]: fieldMapping,
            };
          }, {}),
        };
      }
    };
  };
}

export function FieldMapping(
  fieldMapping: ElasticSearchFieldMapping
): PropertyDecorator {
  return Reflect.metadata(META_DATA_KEY, fieldMapping);
}

/**
 * Provides a typed function to create a properties mapping for the given type.
 * The function itself simply returns the given obj.
 *
 * @param obj The obj to create a mapping for.
 */
export function createSubMapping<T>(
  obj: PropertiesMapping<T>
): PropertiesMapping<T> {
  return obj;
}
