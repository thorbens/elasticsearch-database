import {
  CommonAtomDatabaseOperator,
  CommonDatabaseConditionParser,
  CommonDatabaseOperatorType,
} from "@thorbens/database";
import { cloneOperatorWithPrefix } from "./cloneOperatorWithPrefix";

describe("cloneOperatorWithPrefix", () => {
  it("should clone the operator correctly", () => {
    const operator = new CommonAtomDatabaseOperator(
      CommonDatabaseOperatorType.EQUAL,
      "bar",
      "foo"
    );
    const prefix = "test";
    const parser = new CommonDatabaseConditionParser();
    const cloned = cloneOperatorWithPrefix(operator, prefix, parser);
    expect(cloned.getValue()).toEqual(operator.getValue());
    expect(cloned.getType()).toEqual(operator.getType());
    expect(cloned.getKey()).toEqual(`${prefix}${operator.getKey()}`);
  });
});
