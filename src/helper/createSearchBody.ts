import { ReadOnlyDatabaseQuery } from "@thorbens/database";
import { ElasticSearchDatabaseQuery } from "../ElasticSearchDatabaseQuery";
import { createSorts } from "./createSorts";
import { createQueryAggregations } from "./createQueryAggregations";

/**
 * Creates an elasticsearch search body from the given query.
 *
 * @param query The query to create a search body from.
 */
export function createSearchBody<TModel, TResult>(
  query: ReadOnlyDatabaseQuery<TModel, TResult>
) {
  const filters = query.toObject() as Record<string, unknown>;
  const body: Record<string, unknown> = {
    from: query.getOffset(),
  };
  if (Object.keys(filters).length) {
    body.query = filters;
  }
  if (query instanceof ElasticSearchDatabaseQuery) {
    const postConditions = query.createPostConditions();
    if (Object.keys(postConditions).length) {
      body.post_filter = postConditions;
    }
  }
  if (query.getFields().length > 0) {
    body._source = query.getFields();
  }
  // check order by
  const orderByKey = query.getOrderBy();
  if (orderByKey) {
    body.sort = createSorts(orderByKey, query.getOrderByDirection());
  }
  // check size
  const size = query.getLimit() || null; // use null instead of 0.
  if (size) {
    body.size = size;
  }
  const aggregationFields = query.getAggregationFields();
  if (Object.keys(aggregationFields).length > 0) {
    body.aggs = createQueryAggregations(aggregationFields);
  }

  return body;
}
