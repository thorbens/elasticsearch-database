import { DatabaseOrderByDirection } from "@thorbens/database";

/**
 * Creates a list of sort arguments based on the given orderBy and direction.
 *
 * @param orderBy The order to use.
 * @param direction The direction to use.
 */
export function createSorts(
  orderBy: string,
  direction: DatabaseOrderByDirection = DatabaseOrderByDirection.ASC
) {
  if (!orderBy) {
    return;
  }

  return [
    {
      [orderBy]: direction === DatabaseOrderByDirection.ASC ? `ASC` : `DESC`,
    },
  ];
}
