import {
  DatabaseConditionParser,
  DatabaseOperator,
  isCommonAtomDatabaseOperatorObject,
} from "@thorbens/database";

/**
 * Adds the prefix to the key of the given operator.
 *
 * @param operator The operator to add the prefix to.
 * @param prefix The prefix to add.
 * @param parser The parse to use
 * TODO: make recursive
 */
export function cloneOperatorWithPrefix<TOperator extends DatabaseOperator>(
  operator: TOperator,
  prefix = "",
  parser: DatabaseConditionParser
): TOperator {
  const commonObject = operator.toCommonObject();
  const preferredObject = isCommonAtomDatabaseOperatorObject(commonObject)
    ? {
        ...commonObject,
        key: `${prefix}${commonObject.key}`,
      }
    : commonObject;

  const parsed = parser.parse(preferredObject);
  if (parsed == null) {
    throw new Error(
      `could not clone operator from ${JSON.stringify(preferredObject)}`
    );
  }

  return parsed as TOperator;
}
