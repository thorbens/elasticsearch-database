import { AggregationFields } from "@thorbens/database";
import {
  ElasticSearchRequestAggregations,
  ElasticSearchRequestTermAggregationsOptions,
} from "../Model";

/**
 * @see https://www.elastic.co/guide/en/elasticsearch/reference/current/search-aggregations-bucket-terms-aggregation.html
 */
export type AggregationOptions = Omit<
  ElasticSearchRequestTermAggregationsOptions,
  "field"
>;

const DEFAULT_AGGREGATION_OPTIONS: AggregationOptions = {
  size: 1000,
};

/**
 * Creates an elasticsearch aggregation object for the given aggregation fields.
 *
 * @see https://www.elastic.co/guide/en/elasticsearch/reference/current/search-aggregations.html
 * @param aggregationFields The fields to create aggs for.
 * @param aggregationOptions Options to apply to each aggregation.
 */
export function createQueryAggregations<TModel>(
  aggregationFields: AggregationFields<TModel>,
  aggregationOptions: AggregationOptions = DEFAULT_AGGREGATION_OPTIONS
): Record<string, ElasticSearchRequestAggregations> {
  return Object.entries(aggregationFields).reduce(
    (collector, [name, { field, conditions }]) => {
      const terms = {
        ...aggregationOptions,
        field: field.toString(),
      };

      if (conditions && conditions.length > 0) {
        return {
          ...collector,
          [name]: {
            filter: conditions.reduce(
              (obj, condition) => ({
                ...obj,
                ...condition.toObject(),
              }),
              {}
            ),
            aggs: {
              _filtered: {
                terms,
              },
            },
          },
        };
      }

      return {
        ...collector,
        [name]: {
          terms,
        },
      };
    },
    {}
  );
}
