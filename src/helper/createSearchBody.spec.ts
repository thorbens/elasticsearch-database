import { CommonDatabaseQuery } from "@thorbens/database";
import { createSearchBody } from "./createSearchBody";
import { ElasticSearchDatabaseQuery } from "../ElasticSearchDatabaseQuery";
import { EqualDatabaseOperator } from "../Operators";

describe("createSearchBody", () => {
  it("should create the body correctly if size is 0", () => {
    const query = new CommonDatabaseQuery();
    const body = createSearchBody(query);

    expect(body.size).toBeUndefined();
  });

  it("should create the body correctly if limit and size is defined", () => {
    const query = new CommonDatabaseQuery();
    query.setLimit(10);
    query.setOffset(50);
    const body = createSearchBody(query);

    expect(body.size).toEqual(10);
    expect(body.from).toEqual(50);
  });

  it("should create the body correctly if order is given", () => {
    const query = new CommonDatabaseQuery();
    query.setOrderBy("foo");
    const body = createSearchBody(query);

    expect(body.sort).toEqual([{ foo: "ASC" }]);
  });

  it("should create the body correctly if order is a special keyword", () => {
    const query = new CommonDatabaseQuery();
    query.setOrderBy("_score");
    const body = createSearchBody(query);

    expect(body.sort).toEqual([{ _score: "ASC" }]);
  });

  it("should create the body correctly if single fields are defined", () => {
    const query = new CommonDatabaseQuery();
    query.setFields(["foo", "bar"]);
    const body = createSearchBody(query);

    expect(body._source).toEqual(["foo", "bar"]);
  });

  it("should create the body correctly if post filters are defined", () => {
    const query = new ElasticSearchDatabaseQuery();
    query.addPostCondition(new EqualDatabaseOperator("foo", "bar"));
    const body = createSearchBody(query);

    expect(body.post_filter).toBeDefined();
  });
});
