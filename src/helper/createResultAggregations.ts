import { DatabaseAggregationMap } from "@thorbens/database";
import {
  ElasticSearchResponseBodyAggregation,
  ElasticSearchResponseBodyAggregations,
  ElasticSearchResponseBodyFilteredAggregation,
} from "../Model";

function isAggregation(
  input: unknown
): input is ElasticSearchResponseBodyAggregation {
  return typeof input === "object" && input != null && "buckets" in input;
}

function isFilteredAggregation(
  input: unknown
): input is ElasticSearchResponseBodyFilteredAggregation {
  return typeof input === "object" && input != null && "_filtered" in input;
}

/**
 * Creates an aggregation map from the given elasticsearch aggregations.
 *
 * @param aggregations The aggregations to create a map from.
 */
export function createResultAggregations(
  aggregations: ElasticSearchResponseBodyAggregations
): DatabaseAggregationMap {
  return Object.keys(aggregations).reduce((collector, key) => {
    const aggregation = aggregations[key];
    if (isFilteredAggregation(aggregation)) {
      return {
        ...collector,
        [key]: aggregation._filtered.buckets.reduce(
          (bucketCollector, bucket) => ({
            ...bucketCollector,
            [bucket.key]: bucket.doc_count,
          }),
          {}
        ),
      };
    }
    if (isAggregation(aggregation)) {
      return {
        ...collector,
        [key]: aggregation.buckets.reduce(
          (bucketCollector, bucket) => ({
            ...bucketCollector,
            [bucket.key]: bucket.doc_count,
          }),
          {}
        ),
      };
    }

    return {};
  }, {});
}
