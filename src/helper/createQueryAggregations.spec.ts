import { createQueryAggregations } from "./createQueryAggregations";
import { EqualDatabaseOperator } from "../Operators";

describe("createQueryAggregations", () => {
  it("should create the aggregations correctly", () => {
    const fields = {
      foo: {
        field: "foo",
      },
      fooBar: {
        field: "bar",
      },
    };
    const aggregations = createQueryAggregations(fields);

    expect(aggregations).toEqual({
      foo: {
        terms: {
          field: "foo",
          size: 1000,
        },
      },
      fooBar: {
        terms: {
          field: "bar",
          size: 1000,
        },
      },
    });
  });

  it("should create the aggregations correctly if conditions are present", () => {
    const equalCondition = new EqualDatabaseOperator("foo", "bar");
    const fields = {
      foo: {
        field: "foo",
        conditions: [equalCondition],
      },
    };
    const aggregations = createQueryAggregations(fields);

    expect(aggregations).toEqual({
      foo: {
        filter: equalCondition.toObject(),
        aggs: {
          _filtered: {
            terms: {
              field: "foo",
              size: 1000,
            },
          },
        },
      },
    });
  });
});
