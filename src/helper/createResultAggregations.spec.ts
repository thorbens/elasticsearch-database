import { createResultAggregations } from "./createResultAggregations";

describe("createResultAggregations", () => {
  it("should create the aggregations map correctly", () => {
    const aggregations = {
      source: {
        doc_count_error_upper_bound: 0,
        sum_other_doc_count: 0,
        buckets: [
          {
            key: "Original",
            doc_count: 4726,
          },
          {
            key: "Unknown",
            doc_count: 4572,
          },
        ],
      },
    };
    const aggregationsMap = createResultAggregations(aggregations);

    expect(aggregationsMap).toEqual({
      source: {
        Original: 4726,
        Unknown: 4572,
      },
    });
  });

  it("should create the aggregations map correctly if filters are given", () => {
    const aggregations = {
      source: {
        doc_count: 100,
        _filtered: {
          doc_count_error_upper_bound: 0,
          sum_other_doc_count: 0,
          buckets: [
            {
              key: "Original",
              doc_count: 4726,
            },
            {
              key: "Unknown",
              doc_count: 4572,
            },
          ],
        },
      },
    };
    const aggregationsMap = createResultAggregations(aggregations);

    expect(aggregationsMap).toEqual({
      source: {
        Original: 4726,
        Unknown: 4572,
      },
    });
  });
});
