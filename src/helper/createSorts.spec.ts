import { DatabaseOrderByDirection } from "@thorbens/database";
import { createSorts } from "./createSorts";

describe("createSorts", () => {
  it("should create the sorts correctly", () => {
    const orderBy = "foo";
    const orderByDirection = DatabaseOrderByDirection.DESC;

    const sort = createSorts(orderBy, orderByDirection);
    expect(sort?.length).toEqual(1);
    expect(sort?.[0]).toEqual({
      [orderBy]: "DESC",
    });
  });
});
