import { ApiResponse } from "@elastic/elasticsearch";
import {
  AbstractDatabaseResult,
  DatabaseAggregationMap,
  ReadOnlyDatabaseQuery,
} from "@thorbens/database";
import {
  ElasticSearchResponseBody,
  ElasticSearchResponseBodyHit,
} from "./Model";
import { createResultAggregations } from "./helper";

/**
 * Returns the source element (_source) from the given hit.
 *
 * @param hit The hit to return the source from.
 */
function getSourceFromHit<T>(hit: ElasticSearchResponseBodyHit<T>): T {
  return hit._source;
}

export class ElasticSearchDatabaseResult<
  TSource,
  TResult = TSource
> extends AbstractDatabaseResult<TSource, TResult> {
  private readonly response: ApiResponse<ElasticSearchResponseBody<TResult>>;
  private readonly aggregations: DatabaseAggregationMap;

  public constructor(
    query: ReadOnlyDatabaseQuery<TSource, TResult>,
    response: ApiResponse<ElasticSearchResponseBody<TResult>>
  ) {
    super(query, response.body.hits.hits.map(getSourceFromHit));
    this.response = response;
    this.aggregations = response.body.aggregations
      ? createResultAggregations(response.body.aggregations)
      : {};
  }

  public async getTotalSize(): Promise<number> {
    return this.response.body.hits.total.value;
  }

  public async getAggregations(): Promise<DatabaseAggregationMap> {
    return this.aggregations;
  }
}
