import { ElasticSearchDatabaseResult } from "./ElasticSearchDatabaseResult";
import { CommonDatabaseQuery } from "@thorbens/database";
import { ApiResponse } from "@elastic/elasticsearch";
import { ElasticSearchResponseBody } from "./Model";

describe("ElasticSearchDatabaseResult", () => {
  it("should return the correct total size", async () => {
    const query = new CommonDatabaseQuery();
    const response: ApiResponse<ElasticSearchResponseBody<never>> = {
      body: {
        hits: {
          hits: [],
          total: {
            value: 15,
            relation: "eq",
          },
        },
        took: 0,
        timed_out: false,
      },
      meta: null as never,
      headers: {},
      statusCode: 200,
      warnings: null,
    };
    const result = new ElasticSearchDatabaseResult(query, response);

    expect(await result.getTotalSize()).toEqual(15);
  });

  it("should return the correct aggregations", async () => {
    const query = new CommonDatabaseQuery();
    const response: ApiResponse<ElasticSearchResponseBody<never>> = {
      body: {
        aggregations: {
          foo: {
            doc_count_error_upper_bound: 0,
            sum_other_doc_count: 0,
            buckets: [
              {
                key: "bar",
                doc_count: 10,
              },
            ],
          },
        },
        hits: {
          hits: [],
          total: {
            value: 15,
            relation: "eq",
          },
        },
        took: 0,
        timed_out: false,
      },
      meta: null as never,
      headers: {},
      statusCode: 200,
      warnings: null,
    };
    const result = new ElasticSearchDatabaseResult(query, response);

    expect(await result.getAggregations()).toEqual({
      foo: {
        bar: 10,
      },
    });
  });
});
