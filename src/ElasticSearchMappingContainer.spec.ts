import { ElasticSearchMappingContainer } from "./ElasticSearchMappingContainer";
import { ElasticSearchMapping } from "./mapping";

describe("ElasticSearchMappingContainer", () => {
  it("should return the related fields correctly", () => {
    const mappings: ElasticSearchMapping = {
      properties: {
        bar: {
          type: "long",
        },
        foo: {
          type: "completion",
        },
        zor: {
          type: "keyword",
        },
      },
    };
    const mappingContainer = new ElasticSearchMappingContainer(mappings);

    expect(mappingContainer.getMappings()).toEqual(mappings);
    expect(mappingContainer.getAutocompleteField()).toEqual("foo");
  });
});
