import {
  AbstractDatabaseConditionBuilder,
  AtomDatabaseOperator,
  DatabaseOperator,
  KeyModel,
  LogicalDatabaseOperator,
  SearchDatabaseOperator,
} from "@thorbens/database";
import {
  AndDatabaseOperator,
  EqualDatabaseOperator,
  ExistsDatabaseOperator,
  FullTextDatabaseOperator,
  FullTextOptions,
  GreaterThanDatabaseOperator,
  GreaterThanOrEqualDatabaseOperator,
  InDatabaseOperator,
  LessThanDatabaseOperator,
  LessThanOrEqualDatabaseOperator,
  MatchSomeDatabaseOperator,
  NestedDatabaseOperator,
  NotDatabaseOperator,
  NotEqualDatabaseOperator,
  OrDatabaseOperator,
  RegularExpresionDatabaseOperator,
} from "./Operators";
import {
  BoolDatabaseOperator,
  BoolValues,
} from "./Operators/BoolDatabaseOperator";

/**
 * Elastic Search Implementation of DatabaseConditionBuilder factory.
 */
export class ElasticSearchDatabaseConditionBuilder<
  TModel = {}
> extends AbstractDatabaseConditionBuilder<TModel> {
  public createEqualOperator<TValue = unknown>(
    key: KeyModel<TModel>,
    value: TValue
  ): AtomDatabaseOperator<TModel, TValue> {
    return new EqualDatabaseOperator(key, value);
  }

  public createNotEqualOperator<TValue = unknown>(
    key: KeyModel<TModel>,
    value: TValue
  ): AtomDatabaseOperator<TModel, TValue> {
    return new NotEqualDatabaseOperator(key, value);
  }

  public createNotOperator<TValue = unknown>(
    value: DatabaseOperator<TValue>[]
  ): LogicalDatabaseOperator<TValue> {
    return new NotDatabaseOperator(value);
  }

  public createMatchSomeOperator<TValue = unknown>(
    key: KeyModel<TModel>,
    value: TValue
  ): AtomDatabaseOperator<TModel, TValue> {
    return new MatchSomeDatabaseOperator(key, value);
  }

  public createInOperator<TValue extends unknown[] = []>(
    key: KeyModel<TModel>,
    value: TValue
  ): AtomDatabaseOperator<TModel, TValue> {
    return new InDatabaseOperator(key, value);
  }

  public createRegExpOperator<TValue extends RegExp = RegExp>(
    key: KeyModel<TModel>,
    regExp: TValue
  ): AtomDatabaseOperator<TModel, TValue> {
    return new RegularExpresionDatabaseOperator(key, regExp);
  }

  public createGreaterThanOperator<TValue = unknown>(
    key: KeyModel<TModel>,
    value: TValue
  ): AtomDatabaseOperator<TModel, TValue> {
    return new GreaterThanDatabaseOperator(key, value);
  }

  public createGreaterThanOrEqualOperator<TValue = unknown>(
    key: KeyModel<TModel>,
    value: TValue
  ): AtomDatabaseOperator<TModel, TValue> {
    return new GreaterThanOrEqualDatabaseOperator(key, value);
  }

  public createLessThanOperator<TValue = unknown>(
    key: KeyModel<TModel>,
    value: TValue
  ): AtomDatabaseOperator<TModel, TValue> {
    return new LessThanDatabaseOperator(key, value);
  }

  public createLessThanOrEqualOperator<TValue = unknown>(
    key: KeyModel<TModel>,
    value: TValue
  ): AtomDatabaseOperator<TModel, TValue> {
    return new LessThanOrEqualDatabaseOperator(key, value);
  }

  public createExistsOperator<
    TValue extends DatabaseOperator[] = DatabaseOperator[]
  >(
    key: KeyModel<TModel>,
    value: TValue
  ): AtomDatabaseOperator<TModel, TValue> {
    return new ExistsDatabaseOperator(key, value);
  }

  /**
   * Creates a new {@link BoolDatabaseOperator}.
   *
   * @param value The value to use.
   */
  public createBoolOperator(
    value: BoolValues<TModel>
  ): BoolDatabaseOperator<TModel> {
    return new BoolDatabaseOperator(value);
  }

  public createAndOperator<TValue = unknown>(
    value: DatabaseOperator<TValue>[]
  ): LogicalDatabaseOperator<TValue> {
    return new AndDatabaseOperator(value);
  }

  public createOrOperator<TValue = unknown>(
    value: DatabaseOperator<TValue>[]
  ): LogicalDatabaseOperator<TValue> {
    return new OrDatabaseOperator(value);
  }

  public createFullTextOperator<TValue extends string = string>(
    searchTerm: TValue,
    options?: FullTextOptions
  ): SearchDatabaseOperator<TValue> {
    return new FullTextDatabaseOperator(searchTerm, options);
  }

  public createJoinOperator(
    field: KeyModel<TModel>,
    condition: DatabaseOperator
  ): AtomDatabaseOperator<TModel> {
    return new NestedDatabaseOperator(field, condition);
  }
}
