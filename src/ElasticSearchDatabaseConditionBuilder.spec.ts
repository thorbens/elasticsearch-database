import { ElasticSearchDatabaseConditionBuilder } from "./ElasticSearchDatabaseConditionBuilder";

describe("ElasticSearchDatabaseConditionBuilder", () => {
  it("should create the correct atom operators", () => {
    const operatorBuilder = new ElasticSearchDatabaseConditionBuilder();
    const testKey = `foo`;
    const testValue = `bar`;
    const testRegExp = /foo/;

    const eqOperator = operatorBuilder.createEqualOperator(testKey, testValue);
    expect(eqOperator.getKey()).toEqual(testKey);
    expect(eqOperator.getValue()).toEqual(testValue);

    const matchOperator = operatorBuilder.createMatchSomeOperator(
      testKey,
      testValue
    );
    expect(matchOperator.getKey()).toEqual(testKey);
    expect(matchOperator.getValue()).toEqual(testValue);

    const regExpOperator = operatorBuilder.createRegExpOperator(
      testKey,
      testRegExp
    );
    expect(regExpOperator.getKey()).toEqual(testKey);
    expect(regExpOperator.getValue()).toEqual(testRegExp);
  });
});
