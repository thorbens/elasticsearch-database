import {
  ApiResponse,
  Client as ElasticSearchClient,
  RequestParams,
  errors,
} from "@elastic/elasticsearch";
import {
  DatabaseIDProvider,
  DatabaseModel,
  DatabaseQuery,
  DatabaseResult,
  ReadOnlyDatabaseQuery,
  Repository,
} from "@thorbens/database";
import { Logger } from "@thorbens/logger-model";
import { ElasticSearchDatabaseResult } from "./ElasticSearchDatabaseResult";
import { ElasticSearchMappingContainer } from "./ElasticSearchMappingContainer";
import { ElasticSearchResponseBody } from "./Model";
import { ElasticSearchMapping, ElasticSearchMappingOptions } from "./mapping";
import { createSearchBody } from "./helper";
import { ElasticSearchDatabaseQuery } from "./ElasticSearchDatabaseQuery";

interface AutoCompleteResult {
  id: string;
  text: string;
  score: number;
}

/**
 * Repository for crud operation for the given index name.
 */
export class ElasticSearchRepository<TModel extends DatabaseModel>
  implements Repository<TModel>
{
  private readonly logger: Logger;
  /**
   * The index name used as repository for the given class.
   */
  private readonly indexName: string;
  /**
   * The elastic search client used for requests.
   */
  private readonly client: ElasticSearchClient;
  private readonly mappingContainer: ElasticSearchMappingContainer;
  private readonly mappingOptions: ElasticSearchMappingOptions | undefined;

  public constructor(
    indexName: string,
    client: ElasticSearchClient,
    mapping: ElasticSearchMapping | undefined,
    mappingOptions: ElasticSearchMappingOptions | undefined,
    logger: Logger = console
  ) {
    this.indexName = indexName.toLowerCase();
    this.client = client;
    this.mappingContainer = new ElasticSearchMappingContainer(
      mapping,
      mappingOptions
    );
    this.mappingOptions = mappingOptions;
    this.logger = logger;
  }

  public async updateMappings(): Promise<void> {
    const mappings = this.mappingContainer.getMappings();
    if (!mappings) {
      return;
    }
    this.logger.debug(
      "updating index mapping with the following: %j",
      mappings
    );

    const index = await this.getName();
    let indexExists;
    try {
      const response = await this.client.indices.exists({
        index,
      });
      indexExists = response && response.statusCode == 200 && response.body;
    } catch (ignored) {
      // ignored
    }

    if (indexExists) {
      this.logger.info(`index "${index}" exist. updating existing index.`);

      await this.client.indices.putMapping({
        index,
        body: mappings,
      });

      return;
    }

    const { indexOptions: settings = {} } = this.mappingOptions || {};
    this.logger.info(
      `index "${index}" does not exist. creating empty index with the following options: %j`,
      settings
    );
    await this.client.indices.create({
      index,
      body: {
        mappings,
        settings,
      },
    });
  }

  public async insertOne(element: TModel): Promise<void> {
    this.logger.debug(`insert one element %j`, element);

    await this.client.create(this.createCreateData(element));
  }

  public async insert(elements: ReadonlyArray<TModel>): Promise<void> {
    this.logger.debug(`inserting ${elements.length} elements`);
    const elementsData = elements.flatMap((element) => [
      {
        index: {
          _id: element.id,
          _index: this.indexName,
        },
      },
      element,
    ]);

    await this.client.bulk({
      body: elementsData,
    });
  }

  public async insertOrReplaceOne(element: TModel): Promise<void> {
    this.logger.debug(`inserting or replacing element with id "${element.id}"`);

    await this.client.index(this.createCreateData(element));
  }

  public async replaceOne(element: TModel): Promise<void> {
    this.logger.debug(`replacing element %j`, element);

    await this.client.index(this.createCreateData(element));
  }

  public async updateOneField(
    id: string,
    field: keyof TModel,
    data: TModel[keyof TModel]
  ): Promise<void> {
    this.logger.debug(
      `updating field "${field}" of element with id ${id} element with data: %j`,
      data
    );
    await this.client.update({
      index: this.indexName,
      id,
      body: {
        doc: {
          [field]: data,
        },
      },
    });
  }

  public async updateOne(
    data: Partial<TModel> & DatabaseIDProvider
  ): Promise<void> {
    this.logger.debug(`updating one element %j`, data);
    // update one element
    await this.client.update({
      index: this.indexName,
      id: data.id,
      body: {
        doc: data,
      },
    });
  }

  public update(): Promise<void> {
    this.logger.debug(`updating elements with query`);
    // perform update
    // return this.client.updateByQuery();
    return Promise.reject(`not implemented yet`);
  }

  public async findById(id: string): Promise<TModel | null> {
    this.logger.debug(`finding element with id ${id}`);
    try {
      const response = await this.client.get({
        id,
        index: this.indexName,
      });

      return response.body._source as TModel;
    } catch (e) {
      if (e instanceof errors.ResponseError && e.statusCode === 404) {
        return null;
      }
      // rethrow error
      throw e;
    }
  }

  public async findByQuery<TResult = TModel>(
    query: ReadOnlyDatabaseQuery<TModel, TResult>
  ): Promise<DatabaseResult<TModel, TResult>> {
    const body = this.createSearchBody(query);
    this.logger.debug(`finding elements with body: %j`, body);

    const searchParams: RequestParams.Search<Record<string, unknown>> = {
      track_total_hits: true,
      index: this.indexName,
      body,
    };
    this.logger.debug(`created elastic search prams`, searchParams);

    const response = await this.client.search<
      ElasticSearchResponseBody<TResult>
    >(searchParams);

    return this.createResult(query, response);
  }

  public async findOneByQuery<TResult = TModel>(
    query: ReadOnlyDatabaseQuery<TModel, TResult>
  ): Promise<DatabaseResult<TModel, TResult>> {
    const body = this.createSearchBody(query);
    this.logger.debug(`finding one element with body: %j`, body);
    body.size = 1;

    const response = await this.client.search<
      ElasticSearchResponseBody<TResult>
    >({
      track_total_hits: true,
      index: this.indexName,
      body,
    });

    return this.createResult<TModel, TResult>(query, response);
  }

  public createQuery<TResult = TModel>(): DatabaseQuery<TModel, TResult> {
    return new ElasticSearchDatabaseQuery<TModel, TResult>();
  }

  public async deleteById(id: string): Promise<void> {
    await this.client.delete({
      index: this.indexName,
      id,
    });
  }

  public count(query: ReadOnlyDatabaseQuery<TModel>): Promise<number> {
    const filters = query.toObject();
    this.logger.debug(`counting elements with filters: %j`, filters);
    const body: Record<string, unknown> = {};
    if (Object.keys(filters).length) {
      body.query = filters;
    }

    return this.client
      .count({
        index: this.indexName,
        body,
      })
      .then((response) => response.body.count);
  }

  public async drop(): Promise<void> {
    await this.client.indices.delete({
      index: this.indexName,
    });
  }

  public async deleteByQuery(
    query: ReadOnlyDatabaseQuery<TModel>
  ): Promise<void> {
    const filters = query.toObject();
    this.logger.debug(`deleting elements with filters: %j`, filters);
    const body: Record<string, unknown> = {};
    if (Object.keys(filters).length) {
      body.query = filters;
    }

    await this.client.deleteByQuery({
      index: this.indexName,
      body,
    });
  }

  public async getName(): Promise<string> {
    return this.indexName;
  }

  public async autocomplete(
    input: string,
    maxResults?: number
  ): Promise<AutoCompleteResult[]> {
    const field = this.mappingContainer.getAutocompleteField();
    if (!field) {
      throw new Error("no autocomplete field set.");
    }
    this.logger.debug(`perform autocomplete for "${input}"`);

    const searchParams: RequestParams.Search = {
      index: this.indexName,
      body: {
        suggest: {
          suggestion: {
            prefix: input,
            completion: {
              field,
              size: maxResults || 5,
            },
          },
        },
        _source: false,
      },
    };
    const response = await this.client.search(searchParams);

    return response.body.suggest.suggestion[0].options.map(
      (suggestionEntry: Record<string, unknown>) => ({
        id: suggestionEntry._id,
        text: suggestionEntry.text,
        score: suggestionEntry._score,
      })
    );
  }

  private createResult<TSource, TResult = TSource>(
    query: ReadOnlyDatabaseQuery<TSource, TResult>,
    response: ApiResponse<ElasticSearchResponseBody<TResult>>
  ): DatabaseResult<TSource, TResult> {
    return new ElasticSearchDatabaseResult<TSource, TResult>(query, response);
  }

  /**
   * Creates parameters for indexing a document.
   *
   * @param element The element to index.
   */
  private createCreateData(element: TModel): RequestParams.Create<TModel> {
    return {
      index: this.indexName,
      id: element.id,
      body: element,
    };
  }

  private createSearchBody<TResult>(
    query: ReadOnlyDatabaseQuery<TModel, TResult>
  ) {
    return createSearchBody(query);
  }
}
