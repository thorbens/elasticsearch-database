import {
  AbstractAtomDatabaseOperator,
  CommonDatabaseOperatorType,
} from "@thorbens/database";

/**
 * @see {@link https://www.elastic.co/guide/en/elasticsearch/reference/current/query-dsl-bool-query.html}
 */
export class NotEqualDatabaseOperator<
  TModel,
  TValue = unknown
> extends AbstractAtomDatabaseOperator<TModel, TValue> {
  public readonly type = CommonDatabaseOperatorType.NOT_EQUAL;

  // eslint-disable-next-line @typescript-eslint/ban-types
  public toObject(): object {
    return {
      bool: {
        must_not: {
          term: {
            [this.getKey()]: this.getConditionValue(),
          },
        },
      },
    };
  }

  public getType(): CommonDatabaseOperatorType {
    return this.type;
  }
}
