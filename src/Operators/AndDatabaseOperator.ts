import {
  AbstractLogicalDatabaseOperator,
  CommonDatabaseOperatorType,
} from "@thorbens/database";

/**
 * @see {@link https://www.elastic.co/guide/en/elasticsearch/reference/current/query-dsl-bool-query.html}
 */
export class AndDatabaseOperator<
  TModel
> extends AbstractLogicalDatabaseOperator<TModel> {
  public readonly type = CommonDatabaseOperatorType.AND;

  // eslint-disable-next-line @typescript-eslint/ban-types
  public toObject(): object {
    const value = this.getValue();
    // check if there are more than one condition. if not, simplify query.
    if (Array.isArray(value) && value.length === 1) {
      const conditionValue = this.getConditionValue();

      // simplify condition value again.
      return Array.isArray(conditionValue) && conditionValue.length === 1
        ? conditionValue[0]
        : conditionValue;
    }

    return {
      bool: {
        must: this.getConditionValue(),
      },
    };
  }

  public getType(): CommonDatabaseOperatorType {
    return this.type;
  }
}
