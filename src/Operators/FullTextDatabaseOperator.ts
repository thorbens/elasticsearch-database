import {
  AbstractDatabaseOperator,
  CommonDatabaseOperatorType,
  SearchOptions,
} from "@thorbens/database";

/**
 * Elastic search full text search options.
 *
 * @see {@link https://www.elastic.co/guide/en/elasticsearch/reference/current/query-dsl-match-query.html}
 */
export interface FullTextOptions extends SearchOptions {
  fuzziness?: string;
  operator?: "and" | "or";
  zero_terms_query?: "all" | "none";
  cutoff_frequency?: number;
  auto_generate_synonyms_phrase_query?: boolean;
  type?:
    | "best_fields"
    | "most_fields"
    | "cross_fields"
    | "phrase"
    | "phrase_prefix";
}

/**
 * @see {@link https://www.elastic.co/guide/en/elasticsearch/reference/current/query-dsl-match-query.html}
 */
export class FullTextDatabaseOperator<
  TValue extends string = string
> extends AbstractDatabaseOperator<TValue> {
  public readonly type = CommonDatabaseOperatorType.FULL_TEXT;
  private readonly options: FullTextOptions;

  public constructor(searchTerm: TValue, options?: FullTextOptions) {
    super(searchTerm);
    this.options = Object.assign(this.getDefaultOptions(), options);
  }

  /**
   * Returns the full text search options.
   */
  public getOptions(): FullTextOptions {
    return this.options;
  }

  // eslint-disable-next-line @typescript-eslint/ban-types
  public toObject(): object {
    return {
      multi_match: {
        query: this.getValue(),
        ...(this.getOptions() || {}),
      },
    };
  }

  public getType(): CommonDatabaseOperatorType {
    return this.type;
  }

  /**
   * Returns the default options object to use of no options are given.
   */
  private getDefaultOptions(): FullTextOptions {
    return {
      type: "best_fields",
      fuzziness: "auto",
    };
  }
}
