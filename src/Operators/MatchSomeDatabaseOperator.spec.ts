import { CommonDatabaseOperatorType } from "@thorbens/database";
import { EqualDatabaseOperator } from "./EqualDatabaseOperator";
import { MatchSomeDatabaseOperator } from "./MatchSomeDatabaseOperator";

describe("MatchSomeDatabaseOperator", () => {
  it("should create the operator correctly", () => {
    const value = "bar";
    const key = "foo";
    const condition = new MatchSomeDatabaseOperator(key, value);

    expect(condition.getKey()).toEqual(key);
    expect(condition.getValue()).toEqual(value);

    expect(condition.toObject()).toEqual({
      terms: {
        [key]: value,
      },
    });
    expect(condition.getType()).toEqual(CommonDatabaseOperatorType.MATCH_SOME);
  });

  it("should create the operator correctly for complex values", () => {
    const key = "id";
    const value = "bar";
    const equalCondition = new EqualDatabaseOperator(key, value);
    const matchKey = "foo";
    const condition = new MatchSomeDatabaseOperator(matchKey, equalCondition);

    expect(condition.getKey()).toEqual(matchKey);
    expect(condition.getValue()).toEqual(equalCondition);

    expect(condition.toObject()).toEqual({
      bool: {
        should: [
          {
            term: {
              [`${matchKey}.${key}`]: value,
            },
          },
        ],
      },
    });
    expect(condition.getType()).toEqual(CommonDatabaseOperatorType.MATCH_SOME);
  });

  it("should create the operator correctly with mixed values", () => {
    const key = "id";
    const value = "bar";
    const textValue = "zor";
    const equalCondition = new EqualDatabaseOperator(key, value);
    const matchKey = "foo";
    const matchSomeValue = [equalCondition, textValue];
    const condition = new MatchSomeDatabaseOperator(matchKey, matchSomeValue);

    expect(condition.getKey()).toEqual(matchKey);
    expect(condition.getValue()).toEqual(matchSomeValue);
    expect(condition.toObject()).toEqual({
      bool: {
        should: [
          {
            term: {
              [`${matchKey}.${key}`]: value,
            },
          },
          {
            [matchKey]: textValue,
          },
        ],
      },
    });
    expect(condition.getType()).toEqual(CommonDatabaseOperatorType.MATCH_SOME);
  });
});
