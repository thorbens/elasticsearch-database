import { CommonDatabaseOperatorType } from "@thorbens/database";
import { InDatabaseOperator } from "./InDatabaseOperator";

describe("InDatabaseOperator", () => {
  it("should create the operator correctly", () => {
    const values = ["foo", "bar"];
    const condition = new InDatabaseOperator("id", values);

    expect(condition.getValue()).toEqual(values);
    expect(condition.toObject()).toEqual({
      terms: {
        id: values,
      },
    });
    expect(condition.getType()).toEqual(CommonDatabaseOperatorType.IN);
  });

  it("should create the operator correctly for simple values", () => {
    const values = [1, 2, 3];
    const condition = new InDatabaseOperator("id", values);

    expect(condition.getValue()).toEqual(values);
    expect(condition.toObject()).toEqual({
      terms: {
        id: values,
      },
    });
    expect(condition.getType()).toEqual(CommonDatabaseOperatorType.IN);
  });

  it("should create the operator correctly for an empty value", () => {
    const values: unknown[] = [];
    const condition = new InDatabaseOperator("id", values);

    expect(condition.getValue()).toEqual(values);
    expect(condition.toObject()).toEqual({
      terms: {
        id: values,
      },
    });
    expect(condition.getType()).toEqual(CommonDatabaseOperatorType.IN);
  });
});
