import { CommonDatabaseOperatorType } from "@thorbens/database";
import { AndDatabaseOperator } from "./AndDatabaseOperator";
import { EqualDatabaseOperator } from "./EqualDatabaseOperator";

describe("AndDatabaseOperator", () => {
  it("should create the operator simplified with only one condition", () => {
    const equalOperator = new EqualDatabaseOperator(`foo`, `bar`);
    const values = [equalOperator];
    const condition = new AndDatabaseOperator(values);

    expect(condition.getValue()).toEqual(values);
    expect(condition.toObject()).toEqual(equalOperator.toObject());
    expect(condition.getType()).toEqual(CommonDatabaseOperatorType.AND);
  });

  it("should create the operator correctly", () => {
    const firstEqual = new EqualDatabaseOperator("first", "foo");
    const secondEqual = new EqualDatabaseOperator("second", "bar");
    const values = [firstEqual, secondEqual];
    const condition = new AndDatabaseOperator(values);

    expect(condition.getValue()).toEqual(values);
    expect(condition.toObject()).toEqual({
      bool: {
        must: values.map((value) => value.toObject()),
      },
    });
    expect(condition.getType()).toEqual(CommonDatabaseOperatorType.AND);
  });
});
