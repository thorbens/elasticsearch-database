import { CommonDatabaseOperatorType } from "@thorbens/database";
import { GreaterThanDatabaseOperator } from "./GreaterThanDatabaseOperator";

describe("GreaterThanDatabaseOperator", () => {
  it("should create the operator correctly", () => {
    const key = "foo";
    const value = "bar";
    const condition = new GreaterThanDatabaseOperator(key, value);

    expect(condition.getValue()).toEqual(value);
    expect(condition.getType()).toEqual(CommonDatabaseOperatorType.GT);
    expect(condition.toObject()).toEqual({
      range: {
        [key]: {
          gt: value,
        },
      },
    });
  });
});
