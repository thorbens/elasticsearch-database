import {
  AbstractLogicalDatabaseOperator,
  CommonDatabaseOperatorType,
} from "@thorbens/database";

/**
 * @see {@link https://www.elastic.co/guide/en/elasticsearch/reference/current/query-dsl-bool-query.html}
 */
export class NotDatabaseOperator<T> extends AbstractLogicalDatabaseOperator<T> {
  public readonly type = CommonDatabaseOperatorType.NOT;

  // eslint-disable-next-line @typescript-eslint/ban-types
  public toObject(): object {
    let conditionObject;
    const conditions = this.getConditionValue();
    if (Array.isArray(conditions)) {
      conditionObject = conditions.reduce((object, conditionValue) => {
        return {
          ...object,
          ...conditionValue,
        };
      }, {});
    } else {
      conditionObject = {
        ...conditions,
      };
    }

    return {
      bool: {
        must_not: conditionObject,
      },
    };
  }

  public getType(): CommonDatabaseOperatorType {
    return this.type;
  }
}
