import { CommonDatabaseOperatorType } from "@thorbens/database";
import { LessThanDatabaseOperator } from "./LessThanDatabaseOperator";

describe("LessThanDatabaseOperator", () => {
  it("should create the operator correctly", () => {
    const key = "foo";
    const value = "bar";
    const condition = new LessThanDatabaseOperator(key, value);

    expect(condition.getValue()).toEqual(value);
    expect(condition.getType()).toEqual(CommonDatabaseOperatorType.LT);
    expect(condition.toObject()).toEqual({
      range: {
        [key]: {
          lt: value,
        },
      },
    });
  });
});
