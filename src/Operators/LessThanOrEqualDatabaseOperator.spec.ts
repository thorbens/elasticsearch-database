import { CommonDatabaseOperatorType } from "@thorbens/database";
import { LessThanOrEqualDatabaseOperator } from "./LessThanOrEqualDatabaseOperator";

describe("LessThanOrEqualDatabaseOperator", () => {
  it("should create the operator correctly", () => {
    const key = "foo";
    const value = "bar";
    const condition = new LessThanOrEqualDatabaseOperator(key, value);

    expect(condition.getValue()).toEqual(value);
    expect(condition.getType()).toEqual(CommonDatabaseOperatorType.LTE);
    expect(condition.toObject()).toEqual({
      range: {
        [key]: {
          lte: value,
        },
      },
    });
  });
});
