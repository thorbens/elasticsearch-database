import { CommonDatabaseOperatorType } from "@thorbens/database";
import { NotEqualDatabaseOperator } from "./NotEqualDatabaseOperator";

describe("NotEqualDatabaseOperator", () => {
  it("should create the operator correctly", () => {
    const value = "bar";
    const key = "foo";
    const condition = new NotEqualDatabaseOperator(key, value);

    expect(condition.getKey()).toEqual(key);
    expect(condition.getValue()).toEqual(value);

    expect(condition.toObject()).toEqual({
      bool: {
        must_not: {
          term: {
            [key]: value,
          },
        },
      },
    });
    expect(condition.getType()).toEqual(CommonDatabaseOperatorType.NOT_EQUAL);
  });
});
