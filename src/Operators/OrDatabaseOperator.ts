import {
  AbstractLogicalDatabaseOperator,
  CommonDatabaseOperatorType,
} from "@thorbens/database";

/**
 * @see {@link https://www.elastic.co/guide/en/elasticsearch/reference/current/query-dsl-bool-query.html}
 */
export class OrDatabaseOperator<T> extends AbstractLogicalDatabaseOperator<T> {
  public readonly type = CommonDatabaseOperatorType.OR;

  // eslint-disable-next-line @typescript-eslint/ban-types
  public toObject(): object {
    return {
      bool: {
        should: this.getConditionValue(),
      },
    };
  }

  public getType(): CommonDatabaseOperatorType {
    return this.type;
  }
}
