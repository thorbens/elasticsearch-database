import {
  AbstractAtomDatabaseOperator,
  CommonDatabaseOperatorType,
} from "@thorbens/database";

/**
 * @see {@link https://www.elastic.co/guide/en/elasticsearch/reference/current/query-dsl-range-query.html}
 */
export class GreaterThanOrEqualDatabaseOperator<
  TModel,
  TValue = unknown
> extends AbstractAtomDatabaseOperator<TModel, TValue> {
  public readonly type = CommonDatabaseOperatorType.GTE;

  // eslint-disable-next-line @typescript-eslint/ban-types
  public toObject(): object {
    return {
      range: {
        [this.getKey()]: {
          gte: this.getConditionValue(),
        },
      },
    };
  }

  public getType(): CommonDatabaseOperatorType {
    return this.type;
  }
}
