import { CommonDatabaseOperatorType } from "@thorbens/database";
import { BoolDatabaseOperator } from "./BoolDatabaseOperator";
import { EqualDatabaseOperator } from "./EqualDatabaseOperator";

describe("BoolDatabaseOperator", () => {
  it("should create the operator correctly", () => {
    const firstEqual = new EqualDatabaseOperator("first", "foo");
    const secondEqual = new EqualDatabaseOperator("second", "bar");
    const values = [firstEqual, secondEqual];
    const bool = {
      must: values,
    };
    const condition = new BoolDatabaseOperator(bool);

    expect(condition.getValue()).toEqual(bool);
    expect(condition.toObject()).toEqual({
      bool: {
        must: values.map((value) => value.toObject()),
      },
    });
    expect(condition.getType()).toEqual(CommonDatabaseOperatorType.BOOL);
  });

  it("should create the operator correctly with must and filter", () => {
    const mustEqual = new EqualDatabaseOperator("first", "foo");
    const filterEqual = new EqualDatabaseOperator("second", "bar");
    const bool = {
      must: [mustEqual],
      filter: [filterEqual],
    };
    const condition = new BoolDatabaseOperator(bool);

    expect(condition.getValue()).toEqual(bool);
    expect(condition.toObject()).toEqual({
      bool: {
        must: [mustEqual.toObject()],
        filter: [filterEqual.toObject()],
      },
    });
    expect(condition.getType()).toEqual(CommonDatabaseOperatorType.BOOL);
  });
});
