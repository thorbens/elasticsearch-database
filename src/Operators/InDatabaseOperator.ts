import {
  AbstractAtomDatabaseOperator,
  CommonDatabaseOperatorType,
} from "@thorbens/database";

/**
 * @see {@link https://www.elastic.co/guide/en/elasticsearch/reference/current/query-dsl-terms-query.html}
 */
export class InDatabaseOperator<
  TModel,
  TValue extends unknown[] = []
> extends AbstractAtomDatabaseOperator<TModel, TValue> {
  public readonly type = CommonDatabaseOperatorType.IN;

  // eslint-disable-next-line @typescript-eslint/ban-types
  public toObject(): object {
    return {
      terms: {
        [this.getKey()]: this.getConditionValue(),
      },
    };
  }

  public getType(): CommonDatabaseOperatorType {
    return this.type;
  }
}
