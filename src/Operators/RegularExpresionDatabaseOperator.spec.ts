import { CommonDatabaseOperatorType } from "@thorbens/database";
import { RegularExpresionDatabaseOperator } from "./RegularExpresionDatabaseOperator";

describe("RegularExpresionDatabaseOperator", () => {
  it("should create the operator correctly", () => {
    const value = new RegExp("bar");
    const key = "foo";
    const condition = new RegularExpresionDatabaseOperator(key, value);

    expect(condition.getKey()).toEqual(key);
    expect(condition.getValue()).toEqual(value);

    expect(condition.toObject()).toEqual({
      regexp: {
        [key]: value,
      },
    });
    expect(condition.getType()).toEqual(CommonDatabaseOperatorType.REGEXP);
  });
});
