import { CommonDatabaseOperatorType } from "@thorbens/database";
import { ExistsDatabaseOperator } from "./ExistsDatabaseOperator";

describe("ExistsDatabaseOperator", () => {
  it("should create the operator correctly", () => {
    const value = "bar";
    const key = "foo";
    const condition = new ExistsDatabaseOperator(key, value);

    expect(condition.getKey()).toEqual(key);
    expect(condition.getValue()).toEqual(value);

    expect(condition.toObject()).toEqual({
      exists: {
        [key]: value,
      },
    });
    expect(condition.getType()).toEqual(CommonDatabaseOperatorType.EXISTS);
  });
});
