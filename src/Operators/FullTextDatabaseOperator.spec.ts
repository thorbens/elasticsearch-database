import { CommonDatabaseOperatorType } from "@thorbens/database";
import { FullTextDatabaseOperator } from "./FullTextDatabaseOperator";

describe("FullTextDatabaseOperator", () => {
  it("should create the operator correctly", () => {
    const searchTerm = `foo`;
    const condition = new FullTextDatabaseOperator(searchTerm);

    expect(condition.getValue()).toEqual(searchTerm);
    expect(condition.toObject()).toEqual({
      multi_match: {
        query: searchTerm,
        type: "best_fields",
        fuzziness: "auto",
      },
    });
    expect(condition.getType()).toEqual(CommonDatabaseOperatorType.FULL_TEXT);
  });
});
