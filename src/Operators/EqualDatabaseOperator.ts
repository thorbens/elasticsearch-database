import {
  AbstractAtomDatabaseOperator,
  CommonDatabaseOperatorType,
} from "@thorbens/database";

/**
 * @see {@link https://www.elastic.co/guide/en/elasticsearch/reference/current/query-dsl-term-query.html}
 */
export class EqualDatabaseOperator<
  TModel,
  TValue = unknown
> extends AbstractAtomDatabaseOperator<TModel, TValue> {
  public readonly type = CommonDatabaseOperatorType.EQUAL;

  // eslint-disable-next-line @typescript-eslint/ban-types
  public toObject(): object {
    return {
      term: {
        [this.getKey()]: this.getConditionValue(),
      },
    };
  }

  public getType(): CommonDatabaseOperatorType {
    return this.type;
  }
}
