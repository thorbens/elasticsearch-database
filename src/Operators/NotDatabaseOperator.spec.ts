import { CommonDatabaseOperatorType } from "@thorbens/database";
import { EqualDatabaseOperator } from "./EqualDatabaseOperator";
import { NotDatabaseOperator } from "./NotDatabaseOperator";

describe("NotDatabaseOperator", () => {
  it("should create the operator correctly", () => {
    const value = [new EqualDatabaseOperator("foo", "bar")];
    const condition = new NotDatabaseOperator(value);

    expect(condition.getValue()).toEqual(value);
    expect(condition.toObject()).toEqual({
      bool: {
        must_not: {
          term: {
            foo: "bar",
          },
        },
      },
    });
    expect(condition.getType()).toEqual(CommonDatabaseOperatorType.NOT);
  });
});
