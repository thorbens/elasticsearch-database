import {
  AbstractAtomDatabaseOperator,
  CommonDatabaseOperatorType,
  isCondition,
} from "@thorbens/database";
import { cloneOperatorWithPrefix } from "../helper";
import { ElasticSearchDatabaseConditionParser } from "../ElasticSearchDatabaseConditionParser";
import { ElasticSearchDatabaseConditionBuilder } from "../ElasticSearchDatabaseConditionBuilder";

/**
 * @see {@link https://www.elastic.co/guide/en/elasticsearch/reference/current/query-dsl-terms-query.html}
 */
export class MatchSomeDatabaseOperator<
  TModel,
  TValue = unknown
> extends AbstractAtomDatabaseOperator<TModel, TValue> {
  public readonly type = CommonDatabaseOperatorType.MATCH_SOME;

  // eslint-disable-next-line @typescript-eslint/ban-types
  public toObject(): object {
    const unknownValue = this.getValue();
    const arrayValue = Array.isArray(unknownValue)
      ? unknownValue
      : [unknownValue];
    if (arrayValue.some(isCondition)) {
      const should: unknown[] = [];
      // TODO: initialization should not be necessary at this point
      const parser = new ElasticSearchDatabaseConditionParser(
        new ElasticSearchDatabaseConditionBuilder()
      );
      // filter condition values
      arrayValue.forEach((elem: unknown) => {
        if (isCondition(elem)) {
          should.push(
            cloneOperatorWithPrefix(
              elem,
              `${this.getKey()}.`,
              parser
            ).toObject()
          );
        } else {
          should.push({
            [this.getKey()]: elem,
          });
        }
      });

      return {
        bool: {
          should,
        },
      };
    } else {
      return {
        terms: {
          [this.getKey()]: this.getConditionValue(),
        },
      };
    }
  }

  public getType(): CommonDatabaseOperatorType {
    return this.type;
  }
}
