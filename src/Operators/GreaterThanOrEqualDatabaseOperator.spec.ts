import { CommonDatabaseOperatorType } from "@thorbens/database";
import { GreaterThanOrEqualDatabaseOperator } from "./GreaterThanOrEqualDatabaseOperator";

describe("GreaterThanOrEqualDatabaseOperator", () => {
  it("should create the operator correctly", () => {
    const key = "foo";
    const value = "bar";
    const condition = new GreaterThanOrEqualDatabaseOperator(key, value);

    expect(condition.getValue()).toEqual(value);
    expect(condition.getType()).toEqual(CommonDatabaseOperatorType.GTE);
    expect(condition.toObject()).toEqual({
      range: {
        [key]: {
          gte: value,
        },
      },
    });
  });
});
