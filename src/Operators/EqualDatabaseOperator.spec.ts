import { CommonDatabaseOperatorType } from "@thorbens/database";
import { EqualDatabaseOperator } from "./EqualDatabaseOperator";

describe("EqualDatabaseOperator", () => {
  it("should create the operator correctly", () => {
    const value = "bar";
    const key = "foo";
    const condition = new EqualDatabaseOperator(key, value);

    expect(condition.getKey()).toEqual(key);
    expect(condition.getValue()).toEqual(value);

    expect(condition.toObject()).toEqual({
      term: {
        [key]: value,
      },
    });
    expect(condition.getType()).toEqual(CommonDatabaseOperatorType.EQUAL);
  });

  it("should create the operator correctly for a number", () => {
    const value = 42;
    const key = "foo";
    const condition = new EqualDatabaseOperator(key, value);

    expect(condition.getKey()).toEqual(key);
    expect(condition.getValue()).toEqual(value);

    expect(condition.toObject()).toEqual({
      term: {
        [key]: value,
      },
    });
    expect(condition.getType()).toEqual(CommonDatabaseOperatorType.EQUAL);
  });
});
