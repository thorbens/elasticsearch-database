import {
  AbstractAtomDatabaseOperator,
  CommonDatabaseOperatorType,
} from "@thorbens/database";

/**
 * @see {@link https://www.elastic.co/guide/en/elasticsearch/reference/current/query-dsl-regexp-query.html}
 */
export class RegularExpresionDatabaseOperator<
  TModel,
  TValue extends RegExp = RegExp
> extends AbstractAtomDatabaseOperator<TModel, TValue> {
  public readonly type = CommonDatabaseOperatorType.REGEXP;

  // eslint-disable-next-line @typescript-eslint/ban-types
  public toObject(): object {
    return {
      regexp: {
        [this.getKey()]: this.getValue(),
      },
    };
  }

  public getType(): CommonDatabaseOperatorType {
    return this.type;
  }
}
