import {
  AbstractDatabaseOperator,
  AtomDatabaseOperator,
  CommonDatabaseOperatorType,
  SearchDatabaseOperator,
} from "@thorbens/database";

type AllowedBoolOperators<TModel> =
  | AtomDatabaseOperator<TModel>
  | SearchDatabaseOperator;

/**
 * @see {@link https://www.elastic.co/guide/en/elasticsearch/reference/current/query-dsl-bool-query.html}
 */
export type BoolValues<TModel> = {
  /**
   * The clause (query) must appear in matching documents and will
   * contribute to the score.
   */
  must?: AllowedBoolOperators<TModel>[];
  /**
   * The clause (query) must appear in matching documents.
   * However unlike must the score of the query will be ignored.
   * Filter clauses are executed in filter context, meaning that
   * scoring is ignored and clauses are considered for caching.
   */
  filter?: AllowedBoolOperators<TModel>[];
  /**
   * The clause (query) should appear in the matching document.
   */
  should?: AllowedBoolOperators<TModel>[];
  /**
   * The clause (query) must not appear in the matching documents.
   * Clauses are executed in filter context meaning that scoring is
   * ignored and clauses are considered for caching. Because scoring
   * is ignored, a score of 0 for all documents is returned.
   */
  must_not?: AllowedBoolOperators<TModel>[];
};

/**
 * @see {@link https://www.elastic.co/guide/en/elasticsearch/reference/current/query-dsl-bool-query.html}
 */
export class BoolDatabaseOperator<TModel> extends AbstractDatabaseOperator<
  BoolValues<TModel>
> {
  public readonly type = CommonDatabaseOperatorType.BOOL;

  // eslint-disable-next-line @typescript-eslint/ban-types
  public toObject(): object {
    const values = this.getValue();

    const bool = Object.fromEntries(
      Object.entries(values).map(([key, value]) => {
        if (Array.isArray(value)) {
          return [key, value.map(this.getConditionValueByValue)];
        }

        return [key, this.getConditionValueByValue(value)];
      })
    );

    return {
      bool,
    };
  }

  public getType(): CommonDatabaseOperatorType {
    return this.type;
  }
}
