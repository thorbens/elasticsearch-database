import {
  ElasticSearchFieldMapping,
  ElasticSearchMapping,
  ElasticSearchMappingOptions,
} from "./mapping";

export class ElasticSearchMappingContainer {
  private readonly mapping: ElasticSearchMapping | undefined;
  private mappingOptions: ElasticSearchMappingOptions | undefined;
  private readonly fieldList: ReadonlyArray<{
    field: string;
    mapping: ElasticSearchFieldMapping;
  }>;

  public constructor(
    mapping: ElasticSearchMapping | undefined,
    mappingOptions?: ElasticSearchMappingOptions | undefined
  ) {
    this.mapping = mapping;
    this.mappingOptions = mappingOptions;
    const fieldList = [];
    if (mapping?.properties) {
      for (const field in mapping.properties) {
        if (Object.prototype.hasOwnProperty.call(mapping.properties, field)) {
          fieldList.push({
            field,
            mapping: mapping.properties[field],
          });
        }
      }
    }
    this.fieldList = fieldList;
  }

  public getAutocompleteField(): string | undefined {
    return this.fieldList.find((field) => field.mapping.type === "completion")
      ?.field;
  }

  public getMappings(): ElasticSearchMapping | undefined {
    return this.mapping;
  }
}
