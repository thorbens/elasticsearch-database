import { Client as ElasticSearchClient } from "@elastic/elasticsearch";
import {
  Database,
  DatabaseModel,
  Repository,
  RepositoryConstructor,
} from "@thorbens/database";
import { Logger } from "@thorbens/logger-model";
import { ElasticSearchDatabaseConditionBuilder } from "./ElasticSearchDatabaseConditionBuilder";
import { ElasticSearchDatabaseConditionParser } from "./ElasticSearchDatabaseConditionParser";
import { ElasticSearchDatabaseOptions } from "./Model";
import { ElasticSearchRepository } from "./ElasticSearchRepository";
import { isElasticSearchMappingProvider } from "./mapping";
import { ElasticSearchDatabaseQuery } from "./ElasticSearchDatabaseQuery";

/**
 * Database implementation for a elastic search as database.
 */
export class ElasticSearchDatabase implements Database {
  /**
   * The logger for this class.
   */
  private readonly logger: Logger;
  /**
   * Singleton condition builder.
   */
  private readonly conditionBuilder: ElasticSearchDatabaseConditionBuilder;
  /**
   * Singleton condition parser.
   */
  private readonly conditionParser: ElasticSearchDatabaseConditionParser;
  /**
   * The config for this database.
   */
  private readonly config: ElasticSearchDatabaseOptions;
  /**
   * A map which contains already created repositories.
   * The key maps the repository name to a repository.
   */
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  private readonly repositoryMap: Map<string, Repository<any>> = new Map();
  private client?: ElasticSearchClient;

  public constructor(
    databaseOptions: ElasticSearchDatabaseOptions,
    logger: Logger = console
  ) {
    this.config = databaseOptions;
    this.logger = logger;
    this.conditionBuilder = new ElasticSearchDatabaseConditionBuilder();
    this.conditionParser = new ElasticSearchDatabaseConditionParser(
      this.conditionBuilder
    );
  }

  public async getRepository<T extends DatabaseModel>(
    type: RepositoryConstructor<T>
  ): Promise<Repository<T>> {
    const client = await this.getClient();
    const { mappingOptions, mappings, indexName } =
      this.retrieveMetaDataFromType(type);

    this.logger.debug(`requesting repository for model ${indexName}`);
    if (this.repositoryMap.has(indexName)) {
      this.logger.debug(
        `using already existing repository for model ${indexName}`
      );
      // can not be null
      return this.repositoryMap.get(indexName) as Repository<T>;
    }
    // create new repository
    this.logger.debug(`initializing new repository for model ${indexName}`);

    const repository = new ElasticSearchRepository<T>(
      indexName,
      client,
      mappings,
      mappingOptions,
      this.logger
    );
    // set index mappings
    await repository.updateMappings();
    this.repositoryMap.set(indexName, repository);

    return repository;
  }

  public async dropRepository<TModel extends DatabaseModel>(
    type: RepositoryConstructor<TModel>
  ): Promise<void> {
    const { indexName } = this.retrieveMetaDataFromType(type);
    const repository = await this.getRepository(type);
    await repository.drop();
    this.repositoryMap.delete(indexName);
  }

  public async connect(): Promise<void> {
    // no need to connect
    if (!this.client) {
      this.client = new ElasticSearchClient({
        node: this.config.url,
      });
    }
  }

  public async disconnect(): Promise<void> {
    // no need to disconnect
    if (this.client) {
      delete this.client;
    }
  }

  public createQuery<T>(): ElasticSearchDatabaseQuery<T> {
    return new ElasticSearchDatabaseQuery<T>();
  }

  public getConditionBuilder<
    TModel = unknown
  >(): ElasticSearchDatabaseConditionBuilder<TModel> {
    return this.conditionBuilder;
  }

  public getConditionParser<
    TModel = unknown
  >(): ElasticSearchDatabaseConditionParser<TModel> {
    return this.conditionParser;
  }

  /**
   * Performs a ping.
   * If an error occurred, the promise is rejected.
   */
  public async ping(): Promise<void> {
    const client = await this.getClient();

    await client.ping(
      {},
      {
        requestTimeout: 5000,
      }
    );
  }

  /**
   * Retrieves meta data from the given type.
   * The indexName must be retrievable, otherwise an error is thrown.
   *
   * @param type The type to retrieve meta data from.
   * @throws Error if no index name could be retrieved
   */
  private retrieveMetaDataFromType<TModel extends DatabaseModel>(
    type: RepositoryConstructor<TModel>
  ) {
    const instance = new type();
    let mappings;
    let mappingOptions;
    let indexName: string = type.name;
    if (isElasticSearchMappingProvider(instance)) {
      mappings = instance.getMapping();
      mappingOptions = instance.getMappingOptions();
      if (mappingOptions.indexName) {
        indexName = mappingOptions.indexName;
      }
    }
    if (!indexName) {
      throw new Error("could not get repository name from model class");
    }

    return {
      mappings,
      mappingOptions,
      indexName,
    };
  }

  private async getClient(): Promise<ElasticSearchClient> {
    await this.connect();

    // after connect the client can not be null
    // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
    return this.client!;
  }
}
